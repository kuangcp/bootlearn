package com.myth.bean;

import com.myth.dao.AdminDao;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
@Log4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminTest {

    @Autowired
    AdminDao adminDao;
    @Test
    public void initData(){
        Admin admin = new Admin();
        admin.setAdminName("user");
//        admin.setPassword("12");
        Admin admin1 = adminDao.save(admin);
        System.out.println("新增的id是"+admin1.getAdminId());
    }
    @Test
    public void findByName(){
        Admin admin= adminDao.findByAdminName("user");
        log.info(admin.toString());
    }
}
