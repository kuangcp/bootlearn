package com.myth.bean;

import com.myth.dao.AuthorDao;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
@Log4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorTest {
    @Autowired
    AuthorDao authorDao;
    @Test
    public void initData(){
        Author author = new Author();
        author.setAuthorName("author");
        author.setPassword("34");
        authorDao.save(author);
    }

}
