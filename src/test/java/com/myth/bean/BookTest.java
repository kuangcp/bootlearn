package com.myth.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.junit.Test;

/**
 * Created by Myth on 2017/4/20
 *
 * 使用lombok 方便开发
 */
@Log4j
public class BookTest {
    @Test
    public void init(){


        Book b = new Book(90,"kl","56");
        UI u = this.new UI("op");
        log.info(b.toString()+"\n"+u.toString());
    }

    //内部类
    // 生成密钥对命令
    // keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
    @Data
    @AllArgsConstructor
    class UI{
        private String uid;


    }
}
