package com.myth.bean;

import com.myth.dao.ReaderDao;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
@Log4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReaderTest {
    @Autowired
    ReaderDao readerDao;

    @Test
    public void initData(){
        Reader reader = new Reader();
        reader.setReaderName("read");
        reader.setPassword("1");
        readerDao.save(reader);
    }
}
