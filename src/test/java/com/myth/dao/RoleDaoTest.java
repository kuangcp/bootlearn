package com.myth.dao;

import lombok.extern.log4j.Log4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by mythos on 17-5-10
 * By kuangchengping@outlook.com
 */
@Log4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleDaoTest {

    @Autowired
    AuthorDao authorDao;
    @Autowired
    AdminDao adminDao;
    @Autowired
    ReaderDao readerDao;
//    @Test
//    public void testFindByName(){
//        Author author = authorDao.findByAuthor_name("author");
//        Admin admin = adminDao.findByAdmin_name("myth");
//        Reader reader = readerDao.findByReader_name("read");
//        System.out.println(author.toString());
//        System.out.println(admin.toString());
//        System.out.println(reader.toString());
//    }
}
