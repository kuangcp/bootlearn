package com.myth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@RequestMapping("/sys")
public class DemoApplication {

    private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);
	public static void main(String[] args) throws InterruptedException{

//		log.info("项目正常检查成功"); 为什么会运行两次
	    //默认的启动方式
		SpringApplication.run(DemoApplication.class, args);
//        ApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
//        StringRedisTemplate template = ctx.getBean(StringRedisTemplate.class);
//        CountDownLatch latch = ctx.getBean(CountDownLatch.class);
//
//        log.info("Sending message...");
//        template.convertAndSend("chat", "Hello from Redis!");
//
//        latch.await();

//        System.exit(0);
	}

	@PostMapping("/login")
	public String loginAction(String username,String password,String role){
		System.out.println(username+"|"+password+"|"+role);
		return "/login";
	}

// HTTP 自动转 Https 但是会有端口的转换 等到项目做完了再开启使用


//    @Bean
//    public EmbeddedServletContainerFactory servletContainerFactory() {
//        TomcatEmbeddedServletContainerFactory factory =
//                new TomcatEmbeddedServletContainerFactory() {
//                    @Override
//                    protected void postProcessContext(Context context) {
//                        //SecurityConstraint必须存在，可以通过其为不同的URL设置不同的重定向策略。
//                        SecurityConstraint securityConstraint = new SecurityConstraint();
//                        securityConstraint.setUserConstraint("CONFIDENTIAL");
//                        SecurityCollection collection = new SecurityCollection();
//                        collection.addPattern("/*");
//                        securityConstraint.addCollection(collection);
//                        context.addConstraint(securityConstraint);
//                    }
//                };
//        factory.addAdditionalTomcatConnectors(createHttpConnector());
//        return factory;
//    }


//    private Connector createHttpConnector() {
//        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
//        connector.setScheme("http");
//        connector.setSecure(false);
//        connector.setPort(6666);//http 端口
//        connector.setRedirectPort(8888);// https 端口
//        return connector;
//    }


}
// **********************************************************************
//	//设置通用模板引擎，前后缀
//	@Bean
//	public TemplateResolver templateResolver(){
//		TemplateResolver templateResolver = new ServletContextTemplateResolver();
//		templateResolver.setPrefix("/WEB-INF/templates/");
//		templateResolver.setSuffix(".html");
//		templateResolver.setTemplateMode("HTML5");
//		return templateResolver;
//	}
//	@Bean
//	public SpringTemplateEngine templateEngine(){
//		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//		templateEngine.setTemplateResolver(templateResolver());
//		return templateEngine;
//	}
//
//	public ThymeleafViewResolver thymeleafViewResolver(){
//		ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
//		thymeleafViewResolver.setTemplateEngine(templateEngine());
////		thymeleafViewResolver.setViewClass(ThymeleafView.class);
//		return thymeleafViewResolver;
//	}