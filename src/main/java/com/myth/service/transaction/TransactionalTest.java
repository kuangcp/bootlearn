package com.myth.service.transaction;

import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
public class TransactionalTest {


    // 出现特定的异常就回滚
    @Transactional(rollbackFor = {IndexOutOfBoundsException.class})
    public String actionWithRollback(){
        throw new IndexOutOfBoundsException();
    }

    // 出现特定的异常不回滚
    // 偶或，用IOException就会报错说，需要捕获异常，用这个就不会。？？,不能有两个注解同时存在，说明定制性也不会很强
//    @Transactional(rollbackFor = IOException.class)
    @Transactional(noRollbackFor = IllegalArgumentException.class)
    public String actionWithNoRollback(){
        throw new IllegalArgumentException();
    }
}
