//package com.myth.controller;
//
//import com.myth.bean.Admin;
//import com.myth.bean.AllRoles;
//import com.myth.dao.AdminDao;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by mythos on 17-5-10
// * By kuangchengping@outlook.com
// */
//@Component
//public class MythAuthenticationProvider implements AuthenticationProvider {
//    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//    @Autowired
//    private Environment env;
//    @Autowired
//    AdminDao adminDao;
//
//
////    @Override
////    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
////        System.out.println("detail:实体"+authentication.getDetails());
////        System.out.println("credential:密码"+authentication.getCredentials());
////        System.out.println("Name:用户名"+authentication.getName());
////
////        return null;
////    }
//@Override
//public Authentication authenticate(Authentication authentication)
//        throws AuthenticationException {
//    String username = authentication.getName();
//    String password = (String) authentication.getCredentials();
//    Admin user = adminDao.findByAdminName(username);
//    List<SimpleGrantedAuthority> auths = new ArrayList<>();
//    //游客=》提示用户去注册
//    if(user==null){
//        //授权
//        auths.add(new SimpleGrantedAuthority("ROLE_Admin"));
//        auths.add(new SimpleGrantedAuthority(username));
//        auths.add(new SimpleGrantedAuthority(password));
//        return new UsernamePasswordAuthenticationToken(new Admin(), password, auths);
//    }else{
//        //查询到正确的用户，授权
//        for(AllRoles role:user.getRoles()){
//            auths.add(new SimpleGrantedAuthority(role.getRoleName()));
//        }
//        auths.add(new SimpleGrantedAuthority(username));
//        auths.add(new SimpleGrantedAuthority(password));
//
////        //存在此用户，调用登录接口进行第二道验证
////        String data = HttpRequest.sendGet(env.getProperty("login.url"),"mobile=" + username+"&smsCode="+password);
////        JSONObject json = JSONObject.parseObject(data);
////        if(json.getBoolean("success")==true){
////            //验证码和手机号码正确,返回用户权限
////        }else{
////            //验证消息放到权限里面， 页面提示
////            auths.add(new SimpleGrantedAuthority("ROLE_WRONGCODE"));
////            auths.add(new SimpleGrantedAuthority(username));
////            auths.add(new SimpleGrantedAuthority(password));
////        }
//    }
//    return new UsernamePasswordAuthenticationToken(user, password,auths);
//
//}
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return authentication.equals(UsernamePasswordAuthenticationToken.class);
//    }
//}
