package com.myth.controller;

import com.myth.time.DateFormatUtils;
import lombok.extern.log4j.Log4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by mythos on 17-5-10
 * By kuangchengping@outlook.com
 */
@Log4j
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        log.info("["+authentication.getName()+"]用户登录成功 On"+ DateFormatUtils.toYMDHMS(new Date()));
        System.out.println("登录成功的处理类");
        httpServletResponse.sendRedirect("/myth/test/index");
    }
}
