package com.myth.controller;

import com.myth.bean.Keys;
import com.myth.dao.KeysDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
@RestController
@RequestMapping("/redis")
public class RedisTest {

    @Autowired
    KeysDao keysDao;

    @RequestMapping("/set")
    public void set(){
        Keys keys = new Keys("1","localhost","8080");
        keysDao.save(keys);
        keysDao.stringRedisTemplateDemo();
    }
    @RequestMapping("/getStr")
    public String getStr(){
        return keysDao.getString();
    }

    @RequestMapping("/getKeys")
    public Keys getKeys(){
        return keysDao.getKeys();
    }
}
