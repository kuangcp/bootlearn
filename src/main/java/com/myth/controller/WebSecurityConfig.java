package com.myth.controller;

import com.myth.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Created by Myth on 2017/4/22
 *
 * Security框架的配置类
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDao userDao;
//    @Autowired
//    private MythAuthenticationProvider provider;

//    @Autowired
//    AuthenticationProvider authenticationProvider;

        //配置请求拦截的各个参数
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/").permitAll()//根目录和(!登录页面不进行拦截)登录页面不拦截就会有诡异的问题
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")//未登录用户拦截跳转到的路径
                .defaultSuccessUrl("/test/index")//登录成功后的路径
                .permitAll().successHandler(loginSuccessHandler())
                .permitAll()
                .and()
                .logout()
                .permitAll().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))//定义注销页面
                .logoutSuccessUrl("/login").permitAll().logoutSuccessHandler(logoutSuccessHandler());

//        http.headers().frameOptions().disable();// 禁止iframe 嵌套
        //关闭csrf 防止循环定向
//        http.csrf().disable();
        System.out.println("进入了安全模块");
    }

    //数据库验证
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        System.out.println("进入了");
//        auth.authenticationProvider(provider);
        auth.userDetailsService(new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername (String username) throws UsernameNotFoundException {
                System.out.println("校验得到用户名："+username);
                UserDetails userDetails = userDao.findByUsername(username);//使用JPA进行数据库查询，查到对应的数据和对应的role一致后才可登录
                if (userDetails != null) {
                    return userDetails;
                }
                throw new UsernameNotFoundException("User '" + username + "' not found.");
            }
        });
    }

    // 不受安全模块拦截的路径 模式
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
        web.ignoring().antMatchers("/webjars/**");
        web.ignoring().antMatchers("/img/**");
        web.ignoring().antMatchers("/rest/**");
        web.ignoring().antMatchers("/druid/**");

    }

    @Bean
    public LoginSuccessHandler loginSuccessHandler(){
        return new LoginSuccessHandler();
    }
    @Bean
    public LogoutSuccessHandler logoutSuccessHandler(){
        return new LogoutSuccessHandler();
    }

}
//        http
//            .authorizeRequests()
//            .antMatchers("/","/webjars/**").permitAll()//根目录和(!登录页面不进行拦截)登录页面不拦截就会有诡异的问题
//            .anyRequest().authenticated()//所有的请求都需要进行验证
//            .and()
//            .formLogin()
//                .loginPage("/login")//未登录用户拦截跳转到的路径，也就是下面方法的进入路径
//                .defaultSuccessUrl("/test/index")//登录成功后的路径
//                .failureUrl("/login?error");
//            .permitAll().successHandler(loginSuccessHandler())
//            .and()
//            .logout()
//            .permitAll().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))//定义注销页面
//                .logoutSuccessUrl("/login").permitAll();                //注销成功跳转到登录页面
