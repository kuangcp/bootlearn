package com.myth.controller;

import com.myth.time.DateFormatUtils;
import lombok.extern.log4j.Log4j;
import org.springframework.security.core.Authentication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by mythos on 17-5-15
 * By kuangchengping@outlook.com
 */
@Log4j
public class LogoutSuccessHandler implements org.springframework.security.web.authentication.logout.LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        System.out.println("注销成功");
        if(authentication!=null) {
            log.info(authentication.getName() + "用户注销成功 On " + DateFormatUtils.toYMDHMS(new Date()));
        }else{
            log.info("没有用户在线，注销失败");
        }
        httpServletResponse.sendRedirect("/myth/login");
    }
}
