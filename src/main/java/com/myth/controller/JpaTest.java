package com.myth.controller;

import com.myth.bean.Book;
import com.myth.dao.BookDao;
import com.myth.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 * 分别演示了使用各种方式查询，以及排序分页等使用的方式
 */
@RestController
@RequestMapping("/jpa")
public class JpaTest {

    // 使用构造器模式来自动注入
    final BookDao bookDao;
    final UserDao userDao;

    @Autowired
    public JpaTest(BookDao bookDao, UserDao userDao) {
        this.bookDao = bookDao;
        this.userDao = userDao;
    }

    @RequestMapping(value = "/save")
    public Book save(String name,String address){
        // 怎么做到自动增长
        Book book = bookDao.save(new Book(0,name,address));
        return book;
    }
    @RequestMapping("/test1")
    public List<Book> findByAddress(String address){
        return bookDao.findByAddress(address);
    }
    // 双参数查询，返回单个
    @RequestMapping("/test2")
    public Book findByAddressAndName(String name,String address){
        return bookDao.findByNameAndAddress(name,address);
    }
    // 自定义的双参数 查询
    @RequestMapping("/test3")
    public Book config(String name,String address){
        return bookDao.withNameAndAddressQuery(name,address);
    }
    // 测试 默认双参数 查询返回列表
    @RequestMapping("/test4")
    public List<Book> findNameWithAddress(String name,String address){
        return bookDao.withNameAndAddressNameQuery(name,address);
    }
    // 测试排序
    @RequestMapping("/test5")
    public List<Book> sort(){
        return bookDao.findAll(new Sort(Sort.Direction.DESC,"id"));
    }
    // 测试分页
    @RequestMapping("/test6")
    public Page<Book> page(int page,int size){
        System.out.println("偶");
        // page 当前页数，size是页大小
        return bookDao.findAll(new PageRequest(page,size));
    }
}
