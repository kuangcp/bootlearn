package com.myth.controller;

import com.myth.bean.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myth on 2017/4/13
 * 测试一些框架的使用  Controller默认返回模板名 如果要返回字符串就必须要加上responseBody
 * 测试完成了redis的使用
 * 测试完成了安全模块的使用，
 */
@Controller
@RequestMapping("/test")
public class Hi {

    private Logger log = LoggerFactory.getLogger(Hi.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @ResponseBody
    @RequestMapping("/admin")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public String hi(){
       return "Hello Springboot Just Admin";
    }

    /**
     * 测试使用模板取值
     */
//    @PreAuthorize("hasAnyAuthority('ROLE_READER','ROLE_ADMIN')")
    @PreAuthorize("hasAnyAuthority('ROLE_AUTHOR')")
    @RequestMapping("/index")
    public String index(Model model){

        Book single = new Book(999,"myth","12334234");
        List<Book> books = new ArrayList<>();
        Book book1 = new Book(1,"myth","786576556");
        Book book2 = new Book(2,"li","0909");
        Book book3 = new Book(3,"mythos","0000000");
        books.add(book1);
        books.add(book2);
        books.add(book3);

        model.addAttribute("single",single);
        model.addAttribute("books",books);
        return "index";
    }

    @ResponseBody
    @GetMapping("/redis")
    public String redisTest(){
        //set get
        stringRedisTemplate.opsForValue().set("aaa", "hello redis !!!!!!!!!!");
        String result = stringRedisTemplate.opsForValue().get("aaa");
        //获取所有
//        Set<String> keysList =  stringRedisTemplate.keys("*");
//        for(String temp :keysList){
//            log.info(temp);
//        }
        log.info(result);
        // ops开头的方法对应各种数据类型的各个操作
//        bound：限制
//        opsForValue()
//        opsForHash()
//        opsForList()
//        opsForZSet()
//        opsForCluster()
//        boundGeoOps()
        return result;
    }


}
