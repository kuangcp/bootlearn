package com.myth.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 * 所有角色的基本表格
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AllRoles {
    @Id
    @GeneratedValue
    private Long roleId;
    private String roleName;// ROLE_ 必须以此开头

}
