package com.myth.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Keys implements Serializable{

    // 此类必须用时间序列化借口，因为使用Jackson做序列化需要一个空构造
    private static final long serialVersionUID = 1L;
    private String name;
    private String host;
    private String port;
}
