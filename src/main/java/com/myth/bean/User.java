package com.myth.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Myth on 2017/4/22
 * 实现了UserDetails接口，为了实现权限控制
 *
 */
@Entity
@Data
//@Log4j
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {
    private static final Long serialVersionUID=1L;

    @Id
    @GeneratedValue
    private long id;
    private String username;//只要所有的用户是使用同一个冲突域，用户名作为外键（外键不是在数据库中）
    private String password;
    //某用户拥有的权限集合
    @ManyToMany(cascade = {CascadeType.REFRESH},fetch = FetchType.EAGER)
    private List<AllRoles> roles ;

    // 授予所有的权限
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> auths = new ArrayList<>();
        List<AllRoles> roles = this.getRoles();
        for(AllRoles role:roles){
            System.out.println("授权："+role.getRoleName());
            auths.add(new SimpleGrantedAuthority(role.getRoleName()));
        }
        return auths;
        //授予READER权限
//        return Arrays.asList(new SimpleGrantedAuthority("ROLE_READER"));
    }

    //不过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //不加锁
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //不禁用
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //可用
    @Override
    public boolean isEnabled() {
        return true;
    }
}
