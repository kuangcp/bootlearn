package com.myth.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Myth on 2017/4/20
 * 为了测试jpa的实体类
 */
@Entity
@Data
//@Log4j
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="Book.withNameAndAddressNameQuery",query = "select p from Book p where p.name=?1 and p.address=?2")
// 自定义了一个查询，name指的是方法，后面的是SQL，这个就是自定义的查询了，不得不说很方便
public class Book {
    @Id
    @GeneratedValue
    private long  id;
    private String name;
    private String address;
}
