package com.myth.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 * 多角色安全控制
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reader{
    @Id
    @GeneratedValue
    private Long readerId;
    private String readerName;
    private String password;
}
