package com.myth.dao;

import com.myth.bean.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 * 为了测试redistemplate的使用，使用自动配置类来实现和MYSQL类似的操作
 */
@Repository
public class KeysDao {
    @Autowired
    StringRedisTemplate stringRedisTemplate;
//    @Resource(name="stringRedisTemplate")
    ValueOperations<String,String> valOpsStr ;

    @Autowired
    RedisTemplate<Object,Object>redisTemplate;
//    @Resource(name="redisTemplate")
    ValueOperations<Object,Object> valOps;

    public void save(Keys keys){
//        init();
        valOps.set(keys.getName(),keys);
    }
    public Keys getKeys(){
//        init();
        return (Keys)valOps.get("1");
    }
    public String getString (){
//        init();
        return valOpsStr.get("redisT");
    }
    public void stringRedisTemplateDemo(){
//        init();
        valOps.set("redisT","hello");
    }
    public void init(){
        valOpsStr= stringRedisTemplate.opsForValue();
        valOps = redisTemplate.opsForValue();
    }

}
