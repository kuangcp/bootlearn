package com.myth.dao;

import com.myth.bean.Reader;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
public interface ReaderDao extends JpaRepository<Reader,Long>{
//    Reader findByReader_name(String reader_name);
    Reader findByReaderName(String name);
}
