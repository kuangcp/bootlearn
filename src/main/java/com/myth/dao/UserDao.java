package com.myth.dao;

import com.myth.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Myth on 2017/4/22
 * 感觉是预先写好了很多的抽象方法供选择，只要进行声明了，那么就会在那个动态bean中得到实现
 */
public interface UserDao extends JpaRepository<User,Long>{
    public User findByUsername(String username);
}
