package com.myth.dao;

/**
 * Created by mythos on 17-5-10
 * By kuangchengping@outlook.com
 */
public interface RoleDao<T> {
    public T findByUserName();
}
