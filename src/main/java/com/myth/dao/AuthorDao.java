package com.myth.dao;

import com.myth.bean.Author;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
public interface AuthorDao extends JpaRepository<Author,Long>{
//    Author findByAuthor_name(@Param("admin_name")String author_name);
    Author findByAuthorName(String name);
}
