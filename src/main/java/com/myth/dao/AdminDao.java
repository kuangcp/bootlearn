package com.myth.dao;

import com.myth.bean.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
public interface AdminDao  extends JpaRepository<Admin,Long> {
//    @Query(value="select a from Admin a where a.admin_name=:admin_name")
//    public Admin findByAdmin_name(@Param("admin_name")String admin_name);
    //隐含条件 用户名不能重复
    public Admin findByAdminName(String name);
}
