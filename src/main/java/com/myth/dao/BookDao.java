package com.myth.dao;

import com.myth.bean.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 */
@RepositoryRestResource(path = "book")
public interface BookDao extends JpaRepository<Book,Long>{
    //自定义查询方法接口 原生SQL语句：nativeQuery = true
    @Query(value="select p from Book p where p.name=:name and p.address =:address")
    Book withNameAndAddressQuery(@Param("name")String name, @Param("address")String address);
    // 引用的是实体类上定义的SQL查询语句
    List<Book> withNameAndAddressNameQuery(String name,String address);
    List<Book> findByAddress(String address);
    Book findByNameAndAddress(String name,String address);
}
