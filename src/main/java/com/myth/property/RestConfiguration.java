package com.myth.property;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.ProfileResourceProcessor;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 * Created by mythos on 17-5-6
 * By kuangchengping@outlook.com
 * 使用继承方式来进行配置 ，
 */
@Configuration
public class RestConfiguration extends RepositoryRestMvcConfiguration {
    @Override
    public RepositoryRestConfiguration config() {
        return super.config();
    }
    // 还可以重写别的Config开头的方法进行自定义配置

    @Override
    public ProfileResourceProcessor profileResourceProcessor(RepositoryRestConfiguration config) {
        // 设置rest根目录是应用路径下的路径
        config.setBasePath("/rest");
        return super.profileResourceProcessor(config);

    }

}
