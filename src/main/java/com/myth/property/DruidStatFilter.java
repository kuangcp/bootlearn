//package com.myth.property;
//
//import com.alibaba.druid.support.http.WebStatFilter;
//
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.annotation.WebInitParam;
//
///**
// * Created by mythos on 17-5-21
// * By kuangchengping@outlook.com
// */
//@WebFilter(filterName="druidWebStatFilter",urlPatterns="/*",
//        initParams={
//                @WebInitParam(name="exclusions",value="*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")// 忽略资源
//        })
//public class DruidStatFilter extends WebStatFilter {
//
//}
