package com.spring.condition;

/**
 * Created by mythos on 17-4-26.
 */
public class WindowsListService implements ListService {
    @Override
    public String showListCmd() {
        return "dir";
    }
}
