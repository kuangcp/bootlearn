package com.spring.condition;

/**
 * Created by mythos on 17-4-26.
 * 一个接口，为了接收各种类中的一个
 * 是按条件来实例化的，满足条件就实例化这个bean
 */
public interface ListService {
    public String showListCmd();
}
