package com.spring.task;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by mythos on 17-4-26.
 */
public class AsyncMain {
    //其结果是并发执行而不是顺序执行的
    public static void main(String []a){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TaskExecutorConfig.class);
        AsyncTaskService asyncTaskService = context.getBean(AsyncTaskService.class);
        for (int i = 0; i < 10; i++) {
            asyncTaskService.executeAsyncTask(i);
            asyncTaskService.executeAsyncTaskPlus(i);
        }
        context.close();

    }
}
