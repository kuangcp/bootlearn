package com.spring.annotation.meta;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by mythos on 17-4-26.
 * 自定义组合注解
 */
public class MetaMain {
    public static void main(String []s){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
        DemoService service = context.getBean(DemoService.class);
        service.show();
        context.close();
    }
}
