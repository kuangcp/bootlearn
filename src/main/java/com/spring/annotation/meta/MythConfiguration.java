package com.spring.annotation.meta;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.lang.annotation.*;

/**
 * Created by mythos on 17-4-26
 * By kuangchengping@outlook.com
 * 使用组合注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration//组合
@ComponentScan//组合该注解
public @interface MythConfiguration {
    String [] value() default {};//覆盖value参数
}
