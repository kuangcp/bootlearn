package com.spring.annotation.meta;

import org.springframework.stereotype.Service;

/**
 * Created by mythos on 17-4-26.
 */
@Service
public class DemoService {
    public void show(){
        System.out.println("使用组合注解成功获得bean");
    }
}
