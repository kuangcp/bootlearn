package com.spring.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mythos on 17-4-26.
 * No qualifying bean of type 'org.springframework.scheduling.TaskScheduler' available
 * 虽然有异常抛出，但还是能正常运行 只要添加这个bean就行了
 */
@Service
public class ScheduledTaskService {
    private static final SimpleDateFormat dataFormat=new SimpleDateFormat("HH:mm:ss");


    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime(){
        System.out.println("每隔五秒执行一次"+dataFormat.format(new Date()));
    }

    //在每天的 11：28执行该任务 cron是类Unix下的定时任务
    @Scheduled(cron = "0 57 08 ? * *")
    public void fixTimeExecution(){
        System.out.println("在指定时间 "+dataFormat.format(new Date())+" 执行");
    }
}
